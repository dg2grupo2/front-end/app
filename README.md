<h1 align="center">
     Desafio Final - Equipe 2: ADM tá on!
</h1>

![Badge em Desenvolvimento](http://img.shields.io/static/v1?label=STATUS&message=FINALIZADO&color=GREEN&style=for-the-badge)

Tópicos
=================
- [Tópicos](#tópicos)
  - [💻 Sobre o projeto](#-sobre-o-projeto)
    - [:calling: Interface Web](#calling-interface-web)
  - [Link complemetares](#link-complemetares)
  - [⚙️ Informações Gerais](#️-informações-gerais)
  - [Pré-requisitos](#pré-requisitos)
  - [Editar a aplicação ou rodar localmente](#editar-a-aplicação-ou-rodar-localmente)
  - [🛠 Tecnologias](#-tecnologias)
  - [- NPM](#--npm)
  - [🛠 Ferramentas](#-ferramentas)
  - [🦸 Time de desenvolvimento](#-time-de-desenvolvimento)

## 💻 Sobre o projeto

Esse projeto tem por objetivo de cadastrar administradores para gerenciar pedidos e usuarios do sistema.

Em resumo, o processo se dará através:

- administrador faz seu cadastro no sistema (estilo web) e faz seu login;
- se conecta no sistema com email e senha cadastrado com validação token jwt;
- cada token é instranferivel e uso único.
- ao se conectar irá lista todos os administradores cadastrados;
- terá a opção de buscar um único administrador através do email cadastrado no BD;
- listar todos usuarios cadastrados;
- terá a opção de buscar um único usuario através nome ou id cadastrado no BD;
- verificar detalhes dos dados de usuario;
- alterar usuario ou seus status
- listar todos pedidos cadastrados;
- terá a opção de buscar um único pedido através nome ou id cadastrado no BD;
- buscar pedidos por status;
- verificar detalhes dos dados do pedido;
- status se alteram ao finalizar ou cancelar pedido.

---


### :calling: Interface Web

<p style="display: flex; align-items: flex-start; justify-content: center; flex-wrap: wrap; gap: 10px;">
<img src="./src/assets/tela-cadastro.png" width="200px">
<img src="./src/assets/tela-login.png" width="200px">
<img src="./src/assets/tela-usuarios.png" width="200px">
<img src="./src/assets/edita-usuario.png" width="200px">
<img src="./src/assets/registra-usuario.png" width="200px">
<img src="./src/assets/tela-pedidos.png" width="200px">
<img src="./src/assets/faz-pedido.png" width="200px">
<img src="./src/assets/detalhes-pedido.png" width="200px">
</p>

---

## Link complemetares

link para o repositório da API de administradores no back: [API de administradores](https://gitlab.com/dg2grupo2/back-end/api-administradores)

link para o repositório da API de pedidos no back: [API de pedidos](https://gitlab.com/dg2grupo2/back-end/api-pedidos)

link para o repositório da API de usuarios no back: [API de ususarios](https://gitlab.com/dg2grupo2/back-end/api-ususarios)

link para slide explicativo e esquema do projeto: [Slide](https://docs.google.com/presentation/d/1E25l4NVkKBd4txFzbn1UeNnJnRi1W_Wq-D-KL3r9njY/edit#slide=id.g136204565f9_0_377)

---

## ⚙️ Informações Gerais

## Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git], [Vscode] e [NPM].

## Editar a aplicação ou rodar localmente


```bash

# Clone este repositório em sua máquina  
$ git clone git@gitlab.com:dg2grupo2/front-end/app.git

# Instale as dependências
$ npm i --force

# Agora é só rodar a aplicacão
$ npm start
```
---

## 🛠 Tecnologias
As seguintes linguagens/tecnologias foram usadas na construção do projeto:
- [JavaScript][javaScript]
- [Reactstrap][reactstrap]
- [Vscode][vscode]
- [JIRA][jira]
- [Git][git]
- [React-router][reactrouter]
- [date-fns][date-fns]
- [NPM][npm]
---
## 🛠 Ferramentas

- [JIRA][jira]
- [Draw.io][draw.io]

---

## 🦸 Time de desenvolvimento

⚙️**Carolina Marques** - [Gitlab](https://gitlab.com/carumarques) [Linkedin](https://www.linkedin.com/in/caru-marques/)
⚙️**Silvano Araújo** - [Gitlab](https://gitlab.com/silvanoeng) [Linkedin](https://www.linkedin.com/in/silvano-araujo/)
⚙️**Jean Pierre** - [Gitlab](https://gitlab.com/JeanSisse) [Linkedin](https://www.linkedin.com/in/jeanpierresisse/)
⚙️**Eduardo Gomes** - [Gitlab](https://gitlab.com/eduardo377) [Linkedin](https://www.linkedin.com/in/eduardogomes377/)
⚙️**Ana Beatriz Nunes** - [Gitlab](https://gitlab.com/ananuness) [Linkedin](https://www.linkedin.com/in/ana-beatriz-nunes/)

---

[npm]: https://www.npmjs.com/
[jira]: https://dg2grupo2.atlassian.net/jira/software/projects/DG2GRUP/boards/1
[vscode]: https://code.visualstudio.com/
[reactstrap]: https://www.npmjs.com/package/reactstrap
[javascript]: https://www.ecma-international.org/
[git]:https://git-scm.com/
[reactrouter]:https://reactrouter.com/
[date-fns]: https://date-fns.org/
[draw.io]:https://app.diagrams.net/
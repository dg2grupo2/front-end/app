import { format } from 'date-fns';
import './tablerow.css';

const TableRow = ({ type, user=null, order=null, handleModal=null}) => {
  const obj = user ?? order;
  let statusClass = '';

  switch (obj.status) {
    case 'ativo':
      statusClass = 'active';
      break;
    case 'inativo': 
      statusClass = 'inactive';
      break;
    case 'processando': 
      statusClass = 'processing';
      break;
    case 'confirmado': 
      statusClass = 'conclued';
      break;
    default:
      statusClass = 'canceled';
      break;
  }

  const lastElement = () => {
    return (
      type === 'order' ?
      <div className="order-date">
        <span>
          {format(new Date(obj.dataPedido), "dd/MM/yyyy")}
        </span>
      </div>
      : 
      <div id={String(obj.id)} className="user-edit">
        <span id={String(obj.id)} className="info-details">
          {`(${obj.telefone.substr(0, 2)}) ${obj.telefone.substr(2)}`}
        </span>
      </div>
    );
  }

  return (
    <>
    {type === 'order' && 
      <tr id={String(obj.id)} onClick={(event) => handleModal(event)}>
        <td id={String(obj.id)}>
          <div id={String(obj.id)} className="table-row-title">
            {type === 'order' ? `Pedido nº ${obj.id}` : obj.nome}
          </div>
          <div id={String(obj.id)} className={`status ${statusClass}`}>
            {obj.status}
          </div>
          {lastElement()}
        </td>
      </tr>
    }

    {type === 'user' && 
      <tr id={String(obj.id)} onClick={(event) => handleModal(event)}>
        <td id={String(obj.id)}>
          <div id={String(obj.id)} className="table-row-title">
            {obj.nome}
          </div>
          <div id={String(obj.id)} className={`status ${statusClass}`}>
            {obj.status}
          </div>
          {lastElement()}
        </td>
      </tr>
    }
    </>
  );
}

export default TableRow;
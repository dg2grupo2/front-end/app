import { Modal, Button, ModalHeader, ModalBody } from 'reactstrap';
import './custommodal.css';

const CustomModal = ({ isOpen, setShowModal, title, children, resetErrors, md }) => {
  const handleClick = () => {
    setShowModal(false);
    if (resetErrors) resetErrors();
  }

  return (
    <Modal fullscreen="md" scrollable size={md ? "md" : "lg"} isOpen={isOpen}>
      <Button close onClick={handleClick} />

      <ModalHeader>{title}</ModalHeader>
      <ModalBody>{children}</ModalBody>
    </Modal>
  );
}

export default CustomModal;
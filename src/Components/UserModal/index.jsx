import { useNavigate } from 'react-router';
import useAuth from '../../hooks/useAuth';
import useUser from '../../hooks/useUser';
import useValidation from '../../hooks/useValidation';
import CustomModal from '../CustomModal';
import InputErrorContainer from '../InputErrorContainer';
import CustomInput from '../CustomInput';
import ModalActions from '../ModalActions';
import CustomButton from '../CustomButton';
import './usermodal.css';

const UserModal = ({ type }) => {
  const { 
    userInfo, setUserInfo,
    infoModal, setInfoModal,
    showEditModal, setShowEditModal, 
    showRegisterModal, setShowRegisterModal
  } = useUser();
  const { 
    nameError,emailError,
    cpfError, phoneError,
    isNameValid, isEmailValid,
    isCpfValid, isPhoneValid, resetErrors
  } = useValidation();
  const { getToken } = useAuth();
  const navigate = useNavigate();

  const validateFields = (name, email, cpf, phone='') => {
    const phoneError = isPhoneValid(phone.trim());
    const nameError = isNameValid(name.trim());
    const emailError = isEmailValid(email.trim());
    const cpfError = isCpfValid(cpf.trim());

    return (
      phoneError && nameError && emailError && cpfError 
    );
  }

  const handleRegisterUser = async(event) => {
    event.preventDefault();

    if (!validateFields(userInfo.nome, userInfo.email, userInfo.cpf, userInfo.phone)) 
      return;

    console.log("passei da validacao de registrar usuario")

    try {
      const request = await fetch('http://localhost:8080/usuarios', {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `${getToken()}`
        },
        body: JSON.stringify({
          nome: userInfo.nome,
          email: userInfo.email,
          cpf: userInfo.cpf,
          telefone: userInfo.telefone ? userInfo.telefone : null,
          dataNascimento: userInfo.dataNascimento,
          status: 'ativo'
        })
      });

      const response = await request.json();

      if (response.status === 400) {
        console.log("algo deu errado")
      }

      handleClose();
    } catch (error) {
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  const handleEditUser = async(event) => {
    event.preventDefault();

    if (!validateFields(infoModal.nome, infoModal.email, infoModal.cpf, infoModal.phone)) 
      return;

    try {
      //eslint-disable-next-line
      const request = await fetch(`http://localhost:8080/usuarios`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        },
        body: JSON.stringify({
          id: infoModal.id,
          nome: infoModal.nome,
          email: infoModal.email,
          cpf: infoModal.cpf,
          telefone: infoModal.telefone,
          dataNascimento: infoModal.dataNascimento,
          status: infoModal.status
        })
      });

      handleClose();
    } catch (error) {
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  const handleChangeEditModal = (prop) => (event) => {
    setInfoModal({ 
    ...infoModal, [prop]: event.target.value 
    })
    console.log(infoModal);
  };

  const handleChangeRegisterModal = (prop) => (event) => {
    setUserInfo({ 
    ...userInfo, [prop]: event.target.value 
    })
  };

  const handleClose = () => {
    if (type === 'edit') setShowEditModal(false);
    if (type === 'register') setShowRegisterModal(false);

    resetErrors();
  }

  return (
    <CustomModal
        title={type === 'edit' ? 'Editar usuário' : 'Registrar usuário'} 
        setShowModal={type === 'edit' ? setShowEditModal : setShowRegisterModal} resetErrors={resetErrors}
        isOpen={type === 'edit' ? showEditModal : showRegisterModal}
      >
        <main className="modal-main">
          <InputErrorContainer error={nameError}>
            <CustomInput 
              label="Nome*" id="nome" invalid={nameError.message ? true : false}
              defaultValue={type === 'edit' ? infoModal.nome : userInfo.nome} 
              handleChange={type === 'edit' ? handleChangeEditModal : handleChangeRegisterModal}
            />
          </InputErrorContainer>

          <InputErrorContainer error={emailError}>
            <CustomInput 
              label="Email*" id="email" invalid={emailError.message ? true : false}
              defaultValue={type === 'edit' ? infoModal.email : userInfo.email} 
              handleChange={type === 'edit' ? handleChangeEditModal : handleChangeRegisterModal}
            />
          </InputErrorContainer>

          <InputErrorContainer error={cpfError}>
            <CustomInput 
              label="CPF*" id="cpf" invalid={cpfError.message ? true : false}
              defaultValue={type === 'edit' ? infoModal.cpf : userInfo.cpf} 
              handleChange={type === 'edit' ? handleChangeEditModal : handleChangeRegisterModal}
            />
          </InputErrorContainer>

          <InputErrorContainer error={phoneError}>
            <CustomInput 
              label="Telefone" id="telefone" invalid={phoneError.message ? true : false}
              defaultValue={type === 'edit' ? infoModal.telefone : userInfo.telefone} 
              handleChange={type === 'edit' ? handleChangeEditModal : handleChangeRegisterModal}
            />
          </InputErrorContainer>

          <CustomInput 
            label="Data de nascimento" 
            id="dataNascimento" type="date" 
            defaultValue={type === 'edit' ? infoModal.dataNascimento : userInfo.dataNascimento}
            handleChange={type === 'edit' ? handleChangeEditModal : handleChangeRegisterModal}
          />

          <div className={`modal-checkboxes ${type === 'edit' ? 'show' : 'hide'}`}>
            <div className="checkbox-container">
              <CustomInput 
                label="Ativo" type="checkbox"
                id="status" defaultValue="ativo" 
                handleChange={handleChangeEditModal}
                checked={infoModal.status === 'ativo'} 
              />
            </div>
            <div className="checkbox-container">
              <CustomInput 
                label="Inativo" type="checkbox" 
                id="status" defaultValue="inativo"
                handleChange={handleChangeEditModal}
                checked={infoModal.status === 'inativo'} 
              />
            </div>
          </div>
        </main>

        <ModalActions>
          <CustomButton outline red handleClick={(e) => type === 'edit' ? handleEditUser(e) : handleRegisterUser(e)}>
            {type === 'edit' ? 'CONCLUIR' : 'REGISTRAR'}
          </CustomButton>
          <CustomButton outline handleClick={handleClose}>
            CANCELAR
          </CustomButton>
        </ModalActions>
      </CustomModal>
  );
}

export default UserModal;
import { Alert } from 'reactstrap';
import './alert.css';

const CustomAlert = ({ children }) => (
  <div>
    <Alert>{children}</Alert>
  </div>
);

export default CustomAlert;
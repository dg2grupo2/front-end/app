import { Spinner } from 'reactstrap';
import './loading.css';

const Loading = ({ color='light' }) => 
  <Spinner color={color}>Carregando...</Spinner>;

export default Loading;
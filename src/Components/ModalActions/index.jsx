import styledComponents from 'styled-components';

const Actions = styledComponents.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  gap: 1.5rem;
  margin: 2rem 0;
`

const ModalActions = ({ children }) => <Actions>{children}</Actions>;

export default ModalActions;
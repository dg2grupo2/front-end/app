import { useState } from 'react';
import { Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink, NavbarText } from 'reactstrap';
import menuIcon from '../../assets/menu-icon.svg';
import appIcon from '../../assets/desk-icon.svg';
import './header.css';

const Header = ({ tab="usuarios" }) => {
  const [ collapse, setCollapse ] = useState(false);

  const handleCollapse = () => {
    collapse ? setCollapse(false) : setCollapse(true);
  }

  return (
    <Navbar expand="md">
      <NavbarBrand className='title' href="/usuarios">ADM tá on</NavbarBrand>
      <NavbarToggler onClick={() => handleCollapse()}>
        <span className="navbar-toggler-icon">
          <img src={menuIcon} alt="menu" />
        </span>
      </NavbarToggler>
      <Collapse navbar isOpen={collapse}>
        <Nav className="me-auto" navbar>
          <NavItem>
            <NavLink className={tab === "usuarios" ? "active" : "inactive"} href="/usuarios">
              Usuários
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={tab !== "usuarios" ? "active" : "inactive"} href="/pedidos">
              Pedidos
            </NavLink>
          </NavItem>
        </Nav>
        <NavbarText>
          <span className="navbar-toggler-icon app">
            <img src={appIcon} alt="ícone de mesa" />
          </span>
        </NavbarText>
      </Collapse>
    </Navbar>
  );
}

export default Header;
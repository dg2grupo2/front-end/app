import './inputerrorcontainer.css';

const InputErrorContainer = ({ children, error }) => {
  return (
    <div className="error-container">
      {children}
      {error && <span className="error-span">{error.message}</span>}
    </div>
  );
}

export default InputErrorContainer;
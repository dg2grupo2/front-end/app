import styledComponents from 'styled-components';
import './custombutton.css';

const Button = styledComponents.button`
  all: unset;
  cursor: pointer;
  width: 11.2rem;
  height: 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: 'Montserrat', sans-serif;
  padding: .5rem 2rem;
  font-size: 1rem;
  border-radius: .25rem;
  user-select: none;
  -webkit-user-select: none;
  transition: all ease-out .5s;
`;

const PrimaryButton = styledComponents(Button)`
  letter-spacing: 1.5px;
  color: #FFFFFF;
  background-color: #EA1D2C;
`;

const OutlineButton = styledComponents(Button)`
  letter-spacing: 1.5px;
  color: ${props => props.red ? "#EA1D2C" : "#A6A29F"};
  background-color: #FFFFFF;
  border: 1px solid ${props => props.red ? "#EA1D2C" : "#A6A29F"};
`;

const CustomButton = ({ children, primary, outline, red, handleClick }) => {
  return (
    <>
      {primary && 
        <PrimaryButton className='primary-button' onClick={handleClick}>
          {children}
        </PrimaryButton>}

      {outline && 
        <OutlineButton className={red ? 'outlined-button red' : 'outlined-button'} onClick={handleClick} red={red}>
          {children}
        </OutlineButton>}
    </>
  );
}

export default CustomButton;
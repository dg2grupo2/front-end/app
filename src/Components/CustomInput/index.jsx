import { useState } from 'react';
import CurrencyInput from 'react-currency-input';
import { Input, Label } from 'reactstrap';
import closedEye from '../../assets/hide.png';
import openEye from '../../assets/view.png';
import './custominput.css';

const CustomInput = ({ label, type='text', id, defaultValue, placeholder='', handleChange=null, handleOnKeyDown=null, checked, invalid, maxLength }) => {
  const [ showPassword, setShowPassword ] = useState(false);

  const handleClickPassword = () => showPassword ? 
    setShowPassword(false) : setShowPassword(true);

  const verifyInput = () => {
    if (id === 'senha') {
      return <div className="password-input">
        <Input 
          id={id} type={showPassword ? "text" : "password"} 
          placeholder={placeholder} onChange={handleChange(id)} 
          defaultValue={defaultValue} invalid={invalid} 
          maxLength={32}
        />
        <img 
          onClick={handleClickPassword} 
          src={showPassword ? openEye : closedEye} 
          alt={showPassword ? "olho aberto" : "olho fechado"} 
        />
      </div>
    }
    else if (type === 'date') {
      return <Input 
        id={id} type={type} 
        placeholder={placeholder} 
        onChange={handleChange(id)} 
        defaultValue={defaultValue} 
      />
    }
    else if (type === 'checkbox') {
      return <Input
        type={type} 
        onChange={handleChange(id)} 
        defaultValue={defaultValue}
        checked={checked}
      />
    }
    else if (type === 'currency') {
      return <CurrencyInput 
        id={id} prefix="R$"
        className="form-control"
        value={defaultValue}
        onChangeEvent={handleChange(id)}
      />
    }
    else if (handleChange !== null && type !== 'currency') {
      return <Input 
        id={id} type={type} 
        placeholder={placeholder} 
        onChange={handleChange(id)} 
        defaultValue={defaultValue} 
        invalid={invalid} maxLength={maxLength}
      />
    }
    else if (handleOnKeyDown !== null) {
      return <Input 
        id={id} type={type} 
        placeholder={placeholder} 
        onKeyDown={handleOnKeyDown}
      />
    }
    
    else return <Input 
      id={id} type={type} 
      placeholder={placeholder}
      defaultValue={defaultValue} invalid={invalid} 
    />
  }
  
  return (
    <>
      {label && <Label for={id}>{label}</Label>}

      {verifyInput()}
    </>
  );
}

export default CustomInput;
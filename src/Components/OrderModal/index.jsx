import { useState } from 'react';
import { useNavigate } from 'react-router';
import useAuth from '../../hooks/useAuth';
import useValidation from '../../hooks/useValidation';
import CustomModal from '../CustomModal';
import InputErrorContainer from '../InputErrorContainer';
import CustomInput from '../CustomInput';
import CustomButton from '../CustomButton';
import ModalActions from '../ModalActions';
import './ordermodal.css';

const OrderModal = ({ showOrderModal, setShowOrderModal }) => {
  const { getToken, getAdmId } = useAuth();
  const { userError, isUserValid, priceError, isPriceValid, resetErrors } = useValidation();
  const [ orderInfo, setOrderInfo ] = useState({
    comprador: '', valorTotal: '', descricao: '', dataPedido: ''
  });
  const navigate = useNavigate();

  const validateFields = () => {
    return isUserValid(orderInfo.comprador) && 
      isPriceValid(orderInfo.valorTotal);
  }

  const handleRegisterOrder = async(event) => {
    event.preventDefault();

    if (!validateFields()) return;

    const objUser = await isUserValid(orderInfo.comprador);

    try {
      const request = await fetch('http://localhost:8083/pedidos', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        },
        body: JSON.stringify({
          idAdm: getAdmId(),
          idUsuario: objUser.id,
          email: objUser.email,
          valorTotal: Number(orderInfo.valorTotal.replace('R$', '')),
          descricao: orderInfo.descricao,
          dataPedido: orderInfo.dataPedido
        })
      });

      const response = await request.json();

      handleClose()
    } catch (error) {
      console.log(error.message);
      navigate('/internal_server_error', { replace: true });
    }
  }

  const handleChangeOrder = (prop) => (event) => {
    setOrderInfo({ 
    ...orderInfo, [prop]: event.target.value 
    })
    console.log(orderInfo);
  }

  const handleClose = () => {
    setShowOrderModal(false);
    resetErrors();
  }

  return (
    <CustomModal 
      title="Fazer Pedido" 
      setShowModal={setShowOrderModal} 
      isOpen={showOrderModal} resetErrors={resetErrors}
    >
      <main className="modal-main">
        <InputErrorContainer error={userError}>
          <CustomInput 
            label="Comprador*" 
            id="comprador" invalid={userError.message ? true : false}
            defaultValue={orderInfo.comprador} 
            handleChange={handleChangeOrder}
          />
        </InputErrorContainer>

        <InputErrorContainer error={priceError}>
          <CustomInput 
            label="Valor total*" type="currency"
            id="valorTotal" invalid={priceError.message ? true : false}
            defaultValue={orderInfo.valorTotal} 
            handleChange={handleChangeOrder}
          />
        </InputErrorContainer>

        <CustomInput 
          label="Descrição" 
          id="descricao" type="textarea"
          defaultValue={orderInfo.descricao} 
          handleChange={handleChangeOrder}
          maxLength={10}
        />

        <CustomInput 
          label="Data do pedido" 
          id="dataPedido" type="date" 
          defaultValue={orderInfo.dataPedido}
          handleChange={handleChangeOrder}
        />
      </main>

      <ModalActions>
        <CustomButton outline red handleClick={(e) => handleRegisterOrder(e)}>
          CONCLUIR
        </CustomButton>
        <CustomButton outline handleClick={handleClose}>
          CANCELAR
        </CustomButton>
      </ModalActions>
    </CustomModal>
  );
}

export default OrderModal;
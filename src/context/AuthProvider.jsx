import { createContext } from 'react';
import useAuthState from '../hooks/useAuthState';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const auth = useAuthState();

  return (
    <AuthContext.Provider value={auth}>
      {children}
    </AuthContext.Provider>
  );
}

export default AuthContext;
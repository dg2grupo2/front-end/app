import { createContext } from 'react';
import useUserState from '../hooks/useUserState';

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const user = useUserState();

  return (
    <UserContext.Provider value={user}>
      {children}
    </UserContext.Provider>
  );
}

export default UserContext;
import { createContext } from 'react';
import useAdmState from '../hooks/useAdmState';

const AdmContext = createContext();

export const AdmProvider = ({ children }) => {
  const adm = useAdmState();

  return (
    <AdmContext.Provider value={adm}>
      {children}
    </AdmContext.Provider>
  );
}

export default AdmContext;
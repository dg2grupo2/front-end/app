import { createContext } from 'react';
import useValidationState from '../hooks/useValidationState';

const ValidationContext = createContext();

export const ValidationProvider = ({ children }) => {
  const validations = useValidationState();

  return (
    <ValidationContext.Provider value={validations}>
      {children}
    </ValidationContext.Provider>
  );
}

export default ValidationContext;
import { Routes, Route } from 'react-router';
import { AuthProvider } from './context/AuthProvider';
import { AdmProvider } from './context/AdmProvider';
import { UserProvider } from './context/UserProvider';
import { ValidationProvider } from './context/ValidationProvider';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Users from './pages/Users';
import Orders from './pages/Orders';
import ServerError from './pages/ServerError';
import '../src/app.css';

function App() {
  return (
    <AuthProvider>
      <ValidationProvider>
        <AdmProvider>
          <UserProvider>
            <div className="app">
              <Routes>
                <Route path='/' element={<Login />} />
                <Route path='/cadastro' element={<Signup />} />
                <Route path='/usuarios' element={<Users />} />
                <Route path='/pedidos' element={<Orders />} />
                <Route path='/server_internal_error' element={<ServerError />} />
              </Routes>
            </div>
          </UserProvider>
        </AdmProvider>
      </ValidationProvider>
    </AuthProvider>
  );
}
export default App;
import serverIcon from '../../assets/server-error.svg';
import './servererror.css';

function ServerError() {
  return (
    <div className="container-error">
      <main className="error-main">
        <img className="server-icon" src={serverIcon} alt="erro no servidor" />
        <h1 className="main title">Falha nossa!</h1>
        <p className="main-description">
          <b>Parece que estamos com problemas &#128549;</b>
          <br />
          Tente mais tarde... <br /> Enquanto isso, tome um cafezinho &#9749;
        </p>
      </main>
    </div>
  );
}

export default ServerError;
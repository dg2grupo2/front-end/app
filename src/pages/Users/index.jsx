import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { Table } from 'reactstrap';
import useAuth from '../../hooks/useAuth';
import useUser from '../../hooks/useUser';
import Header from '../../Components/Header';
import CustomInput from '../../Components/CustomInput';
import CustomButton from '../../Components/CustomButton';
import TableRow from '../../Components/TableRow';
import UserModal from '../../Components/UserModal';
import searchIcon from '../../assets/search-icon.svg';
import addIcon from '../../assets/add-icon.svg';
import './users.css';

const Users = () => {
  const { getToken } = useAuth();
  const { 
    setInfoModal,
    showEditModal, setShowEditModal, 
    showRegisterModal, setShowRegisterModal
  } = useUser();
  const [ usersList, setUsersList ] = useState([]);
  const navigate = useNavigate();

  const handleLoadUsersList = async() => {
    try {
      const request = await fetch('http://localhost:8080/usuarios', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        }
      });

      const response = await request.json();

      setUsersList(response);
    } catch (error) {
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  const handleSearchUsers = async(name) => {
    try {
      const request = await fetch(
        `http://localhost:8080/usuarios/busca?nome${name ? '=' + name : ''}`, 
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        }
      });

      const response = await request.json();

      setUsersList(response);
    } catch (error) {
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  const handleOnKeyDown = (event) => {
    if (event.key !== 'Enter') return;

    console.log("nome a ser pesquisado: " + event.target.value)
    handleSearchUsers(event.target.value);
  }

  const handleRegisterModal = () => setShowRegisterModal(true);

  const handleEditModal = (event) => {
    setInfoModal(usersList.find(
      user => user.id === Number(event.target.id)
    ));

    setShowEditModal(true);
  }

  useEffect(() => {
    handleLoadUsersList();
    //eslint-disable-next-line
  }, []);

  return (
    <>
      <Header />

      <div className="table-actions">
        <div className="search-input">
          <img src={searchIcon} alt="lupa" />
          <CustomInput 
            id="buscausuario" placeholder="buscar usuário" 
            handleOnKeyDown={handleOnKeyDown}
          />
        </div>
        <CustomButton primary handleClick={handleRegisterModal}>
          <img className="add-icon" src={addIcon} alt="símbolo de mais" />
          NOVO USUÁRIO
        </CustomButton>
      </div>

      <Table responsive>
        <tbody>
          {usersList ? usersList.map(user =>
            <TableRow key={user.id} type="user" user={user} handleModal={handleEditModal} />
          ): "Nenhum usuário registrado"}
        </tbody>
      </Table>

      {showEditModal && <UserModal type="edit" />}
      {showRegisterModal && <UserModal type="register" />}
    </>
  );
}

export default Users;
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router';
import { format } from 'date-fns';
import { Table } from 'reactstrap';
import useAuth from '../../hooks/useAuth';
import Header from '../../Components/Header';
import CustomButton from '../../Components/CustomButton';
import TableRow from '../../Components/TableRow';
import OrderModal from '../../Components/OrderModal';
import CustomModal from '../../Components/CustomModal';
import addOrder from '../../assets/add-order.svg';
import './orders.css';

const Orders = () => {
  const { getToken } = useAuth();
  const [ ordersList, setOrdersList ] = useState([]);
  const [ showOrderModal, setShowOrderModal ] = useState(false);
  const [ detailsInfo, setDetailsInfo ] = useState({
    valorTotal: '', descricao: '', dataPedido: ''
  });
  const [ showDetailsModal, setShowDetailsModal ] = useState(false);
  const navigate = useNavigate();

  const handleLoadOrdersList = async() => {
    try {
      const request = await fetch('http://localhost:8083/pedidos', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        }
      });

      const response = await request.json();

      setOrdersList(response.content);
    } catch (error) {
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  const handleDetailsModal = (event) => {
    setDetailsInfo(ordersList.find(
      order => order.id === Number(event.target.id)
    ));

    setShowDetailsModal(true);
  }

  useEffect(() => {
    handleLoadOrdersList();
    //eslint-disable-next-line
  }, []);

  return (
    <>
      <Header tab="pedidos" />

      <div className="table-order-actions">
        <CustomButton primary handleClick={() => setShowOrderModal(true)}>
          <img className="add-icon" src={addOrder} alt="carrinho de compras" />
          NOVO PEDIDO
        </CustomButton>
      </div>

      <Table responsive>
        <tbody>
          {ordersList ? ordersList.map(order => 
            <TableRow key={order.id} type="order" order={order} handleModal={handleDetailsModal} />
          ) : "Nenhum pedido registrado"}
        </tbody>
      </Table>

      {showOrderModal && <OrderModal showOrderModal={showOrderModal} setShowOrderModal={setShowOrderModal} />}
      {showDetailsModal && 
        <CustomModal 
          title="Detalhes do pedido" setShowModal={setShowDetailsModal} 
          isOpen={showDetailsModal} md
        >
          <div className="order-details">
            <span>
              <b>Valor total:</b>
              {` R$ ${Number(detailsInfo.valorTotal).toFixed(2)}`}
            </span>
            <br />
            <span>
              <b>Descrição: </b>
              {detailsInfo.descricao}
            </span>
            <br />
            <span>
              <b>Data do pedido: </b>
              {format(new Date(detailsInfo.dataPedido), "dd/MM/yyyy")}
            </span>
          </div>
        </CustomModal>
      }
    </>
  );
}

export default Orders;
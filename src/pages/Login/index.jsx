import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Form, FormGroup } from 'reactstrap';
import useAuth from '../../hooks/useAuth';
import useValidation from '../../hooks/useValidation';
import MainCover from '../../Components/MainCover';
import CustomInput from '../../Components/CustomInput';
import CustomButton from '../../Components/CustomButton';
import CustomAlert from '../../Components/CustomAlert';
import Loading from '../../Components/Loading';

const Login = () => {
  const { setAuthData } = useAuth();
  const { loading, setLoading } = useValidation();
  const [ userLogin, setUserLogin ] = useState({
    email: '', senha: ''
  });
  const [ loginError, setLoginError ] = useState({ message: '' });

  const handleChange = (prop) => (event) => setUserLogin({ 
    ...userLogin, [prop]: event.target.value 
  });
  const navigate = useNavigate();

  const handleLogin = async(e) => {
    e.preventDefault();

    if (!userLogin.email || !userLogin.senha) {
      setLoginError({ message: 'Campos Obrigatórios' });
      return;
    }

    setLoading(true);

    try {
      const request = await fetch('http://localhost:8089/administradores/login', {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(userLogin)
      });
  
      const response = await request.json();

      if (response.status === 401) {
        setLoginError({ message: 'Email e/ou senha inválidos' });
        setLoading(false);
        return;
      }
      
      setAuthData({
        token: response.token,
        id: response.id
      });
      setLoading(false);
      setLoginError({ message: '' });
      navigate('/usuarios', { replace: true });
    } catch(error){
      setLoading(false);
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  return (
    <div className="signup-login-container">
      <MainCover />
    
      <Form className={`initial-forms ${loginError.message ? "invalid" : "valid"}`}>
        {loginError.message && <CustomAlert>{loginError.message}</CustomAlert>}
        
        <FormGroup>
          <CustomInput 
            label="Email" id="email" 
            defaultValue={userLogin.email} handleChange={handleChange}
          />
        </FormGroup>

        <FormGroup>
          <CustomInput 
            label="Senha" id="senha" 
            type="password"
            defaultValue={userLogin.senha} handleChange={handleChange}
          />
        </FormGroup>

        <div className="form-actions">
          <CustomButton handleClick={(e) => handleLogin(e)} primary>
            {loading ? <Loading color="light" /> : 'ENTRAR'}
          </CustomButton>
          <span>
            Ainda não possui conta? {" "}
            <Link className="link" to="/cadastro">Cadastre-se</Link>
          </span>
        </div>
      </Form>
    </div>
  );
}

export default Login;
import { Link, useNavigate } from 'react-router-dom';
import { Form, FormGroup, FormFeedback, FormText } from 'reactstrap';
import useAdm from '../../hooks/useAdm';
import useValidation from '../../hooks/useValidation';
import Loading from '../../Components/Loading';
import MainCover from '../../Components/MainCover';
import CustomInput from '../../Components/CustomInput';
import CustomButton from '../../Components/CustomButton';
import './signup.css';

const Signup = () => {
  const { admData, setAdmData } = useAdm();
  const { 
    nameError, emailError, passwordError, setEmailError, 
    isNameValid, isEmailValid, isPasswordValid,
    loading, setLoading
  } = useValidation();
  const navigate = useNavigate();

  const validateFields = () => {
    const name = isNameValid(admData.nome.trim()); 
    const email = isEmailValid(admData.email.trim()); 
    const password = isPasswordValid(admData.senha);

    return name && email && password;
  }

  const handleChange = (prop) => (event) => setAdmData({ 
    ...admData, [prop]: event.target.value 
  });

  const admRegister = async(e) => {
    e.preventDefault();
    if (validateFields() === false) return;
    
    setLoading(true);

    try {
      const validateEmail = await fetch(
        `http://localhost:8089/administradores/${admData.email}`, 
      {
        method: 'GET'  
      });

      if (validateEmail.status === 200) {
        setEmailError({ message : 'Email já cadastrado' });
        setLoading(false);
        return;
      }

      //eslint-disable-next-line
      const registerRequest = await fetch(
        'http://localhost:8089/administradores/cadastrar', 
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(admData)
      });
      
      setLoading(false);
      setEmailError({ message: '' });
      navigate('/', { replace: true });
    } catch (error) {
      setLoading(false);
      console.log(error.message);
      navigate('/server_internal_error', { replace: true });
    }
  }

  return (
    <div className="signup-login-container">
      <MainCover />

      <Form className="initial-forms">
        <FormGroup>
          <CustomInput 
            label="Nome*" id="nome" 
            defaultValue={admData.nome} handleChange={handleChange} 
            invalid={nameError.message ? true : false} 
          />
          <FormFeedback>
            {nameError.message}
          </FormFeedback>
        </FormGroup>

        <FormGroup>
          <CustomInput 
            label="Email*" id="email" 
            defaultValue={admData.email} handleChange={handleChange} 
            invalid={emailError.message ? true : false} 
          />
          <FormFeedback>
            {emailError.message}
          </FormFeedback>
        </FormGroup>

        <FormGroup>
          <CustomInput 
            label="Senha*" id="senha" 
            type="password"
            defaultValue={admData.senha} handleChange={handleChange} 
            invalid={passwordError.message ? true : false} 
          />
          <FormText className={passwordError.message ? "invalid" : "valid"}>
            Mínimo 6 caracteres
          </FormText>
        </FormGroup>

        <div className="form-actions">
          <CustomButton handleClick={(e) => admRegister(e)} primary>
            {loading ? <Loading color="light" /> : 'CADASTRE-SE'}
          </CustomButton>
          <span>
            Já possui conta? Faça seu <Link className='link' to="/">login</Link>
          </span>
        </div>
      </Form>
    </div>
  );
}

export default Signup;
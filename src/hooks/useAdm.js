import { useContext } from 'react';
import AdmContext from '../context/AdmProvider';

const useAdm = () => useContext(AdmContext);

export default useAdm;
import { useState } from 'react';

const useAdmState = () => {
  const [ admData, setAdmData ] = useState({ nome: '', email: '', senha: '' });

  return {
    admData, setAdmData
  }
}

export default useAdmState;
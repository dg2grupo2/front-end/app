import { useState } from 'react';
import { useNavigate } from 'react-router';
import useAuth from './useAuth';

const useValidationState = () => {
  const [ loading, setLoading ] = useState(false);
  const [ nameError, setNameError ] = useState({ message: '' });
  const [ emailError, setEmailError ] = useState({ message: '' });
  const [ passwordError, setPasswordError ] = useState({ message: '' });
  const [ cpfError, setCpfError ] = useState({ message: '' });
  const [ phoneError, setPhoneError ] = useState({ message: '' });
  const [ userError, setUserError ] = useState({ message: '' });
  const [ priceError, setPriceError ] = useState({ message: '' });
  const { getToken } = useAuth();
  const navigate = useNavigate();

  const isNameValid = (name) => {
    if (!name) {
      setNameError({ message: 'Campo obrigatório' });
      return false;
    }

    if (name.length < 10) {
      setNameError({ message: 'Nome inválido' });
      return false;
    }

    setNameError({ message: '' });
    return true;
  }

  const isEmailValid = (email) => {
    if (!email) {
      setEmailError({ message: 'Campo obrigatório' });
      return false;
    }

    if (!email.includes('@')) {
      setEmailError({ message: 'Email inválido' });
      return false;
    }

    const emailArray = email.split('@');

    if (emailArray[0].length < 3 || emailArray[1].length < 7) {
      setEmailError({ message: 'Email inválido' });
      return false;
    }

    if (emailArray.length < 2 || emailArray.length > 2) {
      setEmailError({ message: 'Email inválido' });
      return false;
    }

    setEmailError({ message: '' });
    return true;
  }

  const isPasswordValid = (password) => {
    if (!password) {
      setPasswordError({ message: 'Campo obrigatório' });
      return false;
    }

    if (password.length < 6) {
      setPasswordError({ message: 'No mínimo 6 caracteres' });
      return false;
    }

    if (password.length > 32) {
      setPasswordError({ message: 'No máximo 32 caracteres' });
      return false;
    }

    setPasswordError({ message: '' });
    return true;
  }

  const isCpfValid = (cpf) => {
    if (!cpf) {
      setCpfError({ message: 'Campo obrigatório' });
      return false;
    }

    if (cpf.length !== 11) {
      setCpfError({ message: 'CPF inválido' });
      return false;
    }

    setCpfError({ message: '' });
    return true;
  }

  const isPhoneValid = (phone) => {
    if ((phone.length !== 11 || phone.length !== 10) && phone !== '') {
      setPhoneError({ 
        message: 'Telefone inválido. Informe o DDD e/ou dígito 9' 
      });
      return false;
    }

    setPhoneError({ message: '' });
    return true;
  }

  const isUserValid = async(user) => {
    let validUser = '';
    const validName = isNameValid(user);

    if (validName === false) {
      setUserError({ message: 'Campo obrigatório' });
      return;
    }
    
    try {
      const request = await fetch(`http://localhost:8080/usuarios/busca?nome=${user}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getToken()}`
        }
      });

      const response = await request.json();
      
      if (response.status === 404) {
        setUserError({ message: 'Usuário não encontrado' });
      }

      validUser = { id: response[0].id, email: response[0].email };
      return validUser;
    } catch (error) {
      console.log(error.message);
      navigate('/internal_server_error', { replace: true });
    }
  }

  const isPriceValid = (price) => {
    if (!price) {
      setPriceError({ message: 'Campo obrigatório' });
      return false;
    }

    setPriceError({ message: '' });
    return true;
  }

  const resetErrors = () => {
    setNameError({ message: '' });
    setEmailError({ message: '' });
    setPasswordError({ message: '' });
    setCpfError({ message: '' });
    setPhoneError({ message: '' });
    setUserError({ message: '' });
    setPriceError({ message: '' });
  }

  return {
    loading, setLoading,
    nameError, setNameError,
    emailError, setEmailError,
    passwordError, setPasswordError,
    cpfError, setCpfError,
    phoneError, setPhoneError,
    userError, setUserError,
    priceError, setPriceError,
    isNameValid, 
    isEmailValid, 
    isPasswordValid,
    isCpfValid,
    isPhoneValid,
    isUserValid,
    isPriceValid,
    resetErrors
  }
}

export default useValidationState;
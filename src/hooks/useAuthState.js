import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router';

const AUTH_KEY = 'app';

const useAuthState = () => {
  const navigate = useNavigate();
  const [ authData, setAuthData ] = useState();

  useEffect(() => {
    if (authData != null) {
      localStorage.setItem(AUTH_KEY, JSON.stringify(authData));
    }
  }, [ authData ]);

  const getValue = (key) => {
    if (authData != null) return authData[key];

    const storage = JSON.parse(localStorage.getItem(AUTH_KEY));

    if (storage != null) {
      setAuthData(storage);
      return JSON.parse(localStorage.getItem(AUTH_KEY))[key];
    }

    return null;
  }

  const getToken = () => getValue('token');

  const getAdmId = () => getValue('id');

  const logout = () => {
    setAuthData({});
    navigate('/', { replace: true });
  }

  return {
    setAuthData,
    getToken,
    getAdmId,
    logout
  }
}

export default useAuthState;
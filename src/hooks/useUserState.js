import { useState } from 'react';

const useUserState = () => {
  const [ userInfo, setUserInfo ] = useState({
    nome: '', cpf: '', telefone: '', email: '', 
    dataNascimento: '', status: ''
  });
  const [ showRegisterModal, setShowRegisterModal ] = useState(false);
  const [ showEditModal, setShowEditModal ] = useState(false);
  const [ infoModal, setInfoModal ] = useState({ 
    id: '', nome: '', cpf: '', telefone: '', 
    email: '', dataNascimento: '', status: ''
  });

  return {
    userInfo,
    setUserInfo,
    showRegisterModal, 
    setShowRegisterModal,
    infoModal, 
    setInfoModal,
    showEditModal, 
    setShowEditModal
  };
}

export default useUserState;
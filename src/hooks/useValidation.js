import { useContext } from 'react';
import ValidationContext from '../context/ValidationProvider';

const useValidation = () => useContext(ValidationContext);

export default useValidation;